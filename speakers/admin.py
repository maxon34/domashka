from django.contrib import admin

from .models import speakers

class AuthorAdmin(admin.ModelAdmin):
    search_fields = ['last_name']
    pass
admin.site.register(speakers, AuthorAdmin)

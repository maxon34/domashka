from django.shortcuts import render
from .models import organizations
def index(request):
 organizations_list = organizations.objects.all()
 context = {'organizations': organizations_list}
 return render(request, 'organizations/index.html', context)

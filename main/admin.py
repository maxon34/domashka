from django.contrib import admin
from .models import main
class AuthorAdmin(admin.ModelAdmin):
    search_fields = ['title']
    pass
admin.site.register(main, AuthorAdmin)



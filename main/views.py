from django.shortcuts import render
from .models import main
def index(request):
 main_list = main.objects.all()
 context = {'main': main_list}
 return render(request, 'main/index.html', context)
